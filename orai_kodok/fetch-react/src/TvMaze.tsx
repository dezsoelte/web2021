import { useEffect, useState } from "react";
import { IShow } from "./shared";
import ShowCard from "./ShowCard";
import styles from "./TvMaze.module.scss"



export default function TvMaze() {
  const [data, setData] = useState<IShow[]>([])
  const [query, setQuery] = useState("");
  function loadData() {
    fetch("https://api.tvmaze.com/search/shows?q=" + query)
      .then((r) => r.json())
      .then(data => setData(data))
  }

  useEffect(() => {
    loadData();
  }, [query])

  return (
    <div>
      <input type="text" value={query} onChange={(e) => setQuery(e.target.value)} />
      <div className={styles.container}>

        {data.map(a => ShowCard(a))}
      </div>
    </div>
  );
}