import { IShow } from "./shared";
import styles from "./ShowCard.module.scss"
export default function ShowCard(show: IShow) {
    return <div key={show.show.id} className={styles.card}>
        {show.show.image?.medium && <img src={show.show.image?.medium} />}
        {show.show.name}
    </div>
}