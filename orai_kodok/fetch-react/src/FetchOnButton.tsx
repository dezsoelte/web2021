import { useState } from 'react';
import './App.css';
import { AllatRow, IAllat } from './shared';

function FetchOnButton() {
  const [allatok, setAllatok] = useState<IAllat[]>([])

  function loadData() {
    fetch("http://localhost:8080/api/allat")
      .then((r) => r.json())
      .then(data => setAllatok(data))
  }

  return (
    <div className="App">
      <button onClick={loadData}>load</button>

      <table><tbody>
        {allatok.map(a => AllatRow(a))}
      </tbody></table>
    </div>
  );
}

export default FetchOnButton;
