export interface IAllat {
  id: number,
  weight: number,
  name: string
}

export function AllatRow(allat: IAllat) {
  return <tr key={allat.id}>
    <td>{allat.id}</td>
    <td>{allat.name}</td>
    <td>{allat.weight}</td>
  </tr>
}

export interface IShow{
  score:number,
  show:{
      id:number,
      name:string,
      image:{
          medium?:string
      }
      rating:{
          average:number
      }
  }
}

