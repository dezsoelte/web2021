import { useEffect, useState } from "react";
import { AllatRow, IAllat } from "./shared";

export default function FetchOnLoad(){

    const [allatok, setAllatok] = useState<IAllat[]>([])

    function loadData() {
      fetch("http://localhost:8080/api/allat")
        .then((r) => r.json())
        .then(data => setAllatok(data))
    }

    useEffect(()=>{
        loadData();
    },[])
  
    return (
      <div className="App">
        <table><tbody>
          {allatok.map(a => AllatRow(a))}
        </tbody></table>
      </div>
    );
}