window.addEventListener("load", () => {

    document.getElementById("button").addEventListener("click", () => {
        const kereseiParameter = document.getElementById("search").value;


        fetch("https://api.tvmaze.com/singlesearch/shows?q=" + kereseiParameter)
            .then(response => response.json())
            .then(data => {
                const rootElement = document.getElementById("root");

                const image = document.createElement("img");
                image.src = data.image.medium;

                const name = document.createElement("h1");
                name.innerText = data.name;

                rootElement.appendChild(name);
                rootElement.appendChild(image);
            }).catch((reason) => {
                console.log(reason.message)
            })
    })
})

