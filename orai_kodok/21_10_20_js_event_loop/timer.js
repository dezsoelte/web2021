window.addEventListener("load", () => {

    const startButton = document.getElementById("start");
    const idoElem = document.getElementById("idozito");

    let ido = 0;
    let idozito;

    startButton.addEventListener("click",() => {
        // undefined false 0 []
        if(idozito){
            clearInterval(idozito)
            idozito = null;
        }else{
            idozito = setInterval(() => {
                idoElem.innerText = ido;
                ido++;
            }, 1000)
        }
    })

})