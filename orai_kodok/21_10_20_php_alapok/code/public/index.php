<?php
$egesz = 1;
$szoveg = "asd";
$lebego = 1.1;

$tomb=[
    "alma",
    "körte",
    "asd",
];

$objektumTomb=[
    [
        "nev"=>"Csúnya",
        "kor"=>17,
        "labmeret"=>45
    ],
    [
        "nev"=>"Asd",
        "kor"=>123,
        "labmeret"=>12
    ],
    [
        "nev"=>"Asd2",
        "kor"=>1233,
        "labmeret"=>124
    ]
];



// echo $egesz;
// echo "<br>";
// echo($szoveg);
// echo "<br>";
// print $lebego;
// echo "<br>";
// print($lebego);

function negyzet(int $num)
{
    return $num * $num;
}



// echo(negyzet(2))
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <?php
        echo (negyzet(2));
        ?>
    </div>
    <div>
        <?= $szoveg ?>
    </div>

    <div>
        <?php 
            foreach($tomb as $elem){
                echo($elem);
            }
        ?>
    </div>

    <table>
        <tbody>
            <?php
                foreach($objektumTomb as $elem):
            ?>
            <tr>
                <td><?= $elem["nev"]?></td>
                <td><?= $elem["kor"]?></td>
                <td><?= $elem["kor"] < 18 ? "kiskoru" : "nagykoru" ?></td>
                <!-- ?= ugyanaz mint ?php echo -->
                <td><?php echo $elem["labmeret"];?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>


    <?php 
        $szamok=[1,2,3,4];
        $ketszersSzamok = array_map(function($elem){return $elem*2;}, $szamok);
        foreach($ketszersSzamok as $szam){
            echo $szam."<br>";
        }

        var_dump($ketszersSzamok);

        $szurt = array_filter($szamok,function($szam){
            return $szam>2;
        });
        echo "<br>";
        var_dump($szurt);
    ?>
</body>

</html>