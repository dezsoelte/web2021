<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    #[Route("/", name: "index")]
    public function index(): Response
    {
        return $this->render("base.html.twig");
    }

    #[Route("/hello", name: "hello")]
    public function hello(): Response
    {
        return $this->render("hello/index.html.twig");
    }


    #[Route("/hello/{name}",  name: "hello_name")]
    public function hello2(string $name): Response
    {
        return $this->render(
            "hello/index.html.twig",
            ["name" => $name]
        );
    }


    #[Route("/hello3", name: "hello3")]
    public function hello3(): Response
    {
        return new Response("Hello");
    }

    #[Route("/hello4", name: "hello4")]
    public function hello4(): JsonResponse
    {
        return new JsonResponse(["nev"=>"Potyi", "asd"=>2, "list"=>[1,2,3]]);
    }

}
