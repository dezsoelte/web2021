<?php

namespace App\Controller;

use App\Repository\AllatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/api")]
class ApiController extends AbstractController
{
    #[Route("/allat", name: "allat_api_index")]
    function api_index(AllatRepository $repo): JsonResponse
    {
        $allatok = $repo->getAllArray();
        return new JsonResponse($allatok);
    }

}
