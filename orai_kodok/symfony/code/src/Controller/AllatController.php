<?php

namespace App\Controller;

use App\Entity\Allat;
use App\Entity\Gazda;
use App\Form\AllatFormType;
use App\Repository\AllatRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/allat")]
class AllatController extends AbstractController
{

    #[Route("/", name: "allat_index")]
    function index(EntityManagerInterface $entityManager): Response
    {
        $repo = $entityManager->getRepository(Allat::class);
        $allatok = $repo->findAll();


        return $this->render(
            "allat/index.html.twig",
            ["allatok" => $allatok]
        );
    }


    #[Route("/edit/{allat}", name: "allat_edit")]
    function edit(Request $request, EntityManagerInterface $entityManager, ?Allat $allat = null): Response
    {
        $allat = $allat ?? new Allat();
        $form = $this->createForm(AllatFormType::class, $allat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $allat = $form->getData();
            $entityManager->persist($allat);
            $entityManager->flush();
            return new RedirectResponse($this->generateUrl("allat_index"));
        }

        return $this->render("allat/edit.html.twig", ["form" => $form->createView()]);
    }




    #[Route("/gazda/{gazda}", name: "allat_gazda")]
    function gazda(Request $request, EntityManagerInterface $entityManager, Gazda $gazda): Response
    {
        $allat = new Allat();
        $allat->setGazda($gazda);
        $form = $this->createForm(AllatFormType::class, $allat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $allat = $form->getData();
            $entityManager->persist($allat);
            $entityManager->flush();
            return new RedirectResponse($this->generateUrl("allat_index"));
        }

        return $this->render("allat/edit.html.twig", ["form" => $form->createView()]);
    }

    #[Route("/delete/{allat}", name: "allat_delete")]
    function delete(EntityManagerInterface $entityManager, Allat $allat): Response
    {
        $entityManager->remove($allat);
        $entityManager->flush();
        return new RedirectResponse($this->generateUrl("allat_index"));
    }



}
