<?php

namespace App\Controller;

use App\Entity\Gazda;
use App\Form\GazdaType;
use App\Repository\GazdaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/gazda')]
class GazdaController extends AbstractController
{
    #[Route('/', name: 'gazda_index', methods: ['GET'])]
    public function index(GazdaRepository $gazdaRepository): Response
    {
        return $this->render('gazda/index.html.twig', [
            'gazdas' => $gazdaRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'gazda_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $gazda = new Gazda();
        $form = $this->createForm(GazdaType::class, $gazda);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($gazda);
            $entityManager->flush();

            return $this->redirectToRoute('gazda_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gazda/new.html.twig', [
            'gazda' => $gazda,
            'form' => $form,
        ]);
    }

    #[Route("/api", name: "gazda_api_index")]
    function api_index(GazdaRepository $repo): JsonResponse
    {
        $allatok = $repo->getAllArray();
        return new JsonResponse($allatok);
    }

    #[Route('/{id}', name: 'gazda_show', methods: ['GET'])]
    public function show(Gazda $gazda): Response
    {
        return $this->render('gazda/show.html.twig', [
            'gazda' => $gazda,
        ]);
    }

    #[Route('/{id}/edit', name: 'gazda_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Gazda $gazda, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(GazdaType::class, $gazda);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('gazda_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gazda/edit.html.twig', [
            'gazda' => $gazda,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'gazda_delete', methods: ['POST'])]
    public function delete(Request $request, Gazda $gazda, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$gazda->getId(), $request->request->get('_token'))) {
            $entityManager->remove($gazda);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gazda_index', [], Response::HTTP_SEE_OTHER);
    }


}
