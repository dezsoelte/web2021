<?php

namespace App\Repository;

use App\Entity\Kezeles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Kezeles|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kezeles|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kezeles[]    findAll()
 * @method Kezeles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KezelesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kezeles::class);
    }

    // /**
    //  * @return Kezeles[] Returns an array of Kezeles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Kezeles
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
