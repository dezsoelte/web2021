<?php

namespace App\Repository;

use App\Entity\Allat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Allat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Allat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Allat[]    findAll()
 * @method Allat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AllatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Allat::class);
    }

    // /**
    //  * @return Allat[] Returns an array of Allat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Allat
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getAllArray()
    {
        return $this
            ->createQueryBuilder('a')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
