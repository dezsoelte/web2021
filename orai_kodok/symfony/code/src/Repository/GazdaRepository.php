<?php

namespace App\Repository;

use App\Entity\Gazda;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gazda|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gazda|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gazda[]    findAll()
 * @method Gazda[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GazdaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gazda::class);
    }

    // /**
    //  * @return Gazda[] Returns an array of Gazda objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Gazda
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getAllArray()
    {
        return $this
            ->createQueryBuilder('g')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
