<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity()]
class Allat{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type:"integer")]
    private int $id;

    #[ORM\Column(type:"string",length:255)]
    #[Assert\NotBlank()]
    #[Assert\Length(min:2, max:255)]
    private string $name;

    #[ORM\Column(type:"integer")]
    #[Assert\GreaterThan(0)]
    private int $weight;

    #[ORM\ManyToOne(targetEntity:'Gazda', inversedBy:'allatok')]
    private ?Gazda $gazda=null;

    #[ORM\OneToMany(mappedBy: 'allat', targetEntity: Kezeles::class, orphanRemoval: true)]
    private $kezelesek;

    public function __construct()
    {
        $this->kezelesek = new ArrayCollection();
    }

    public function getId():int{
        return $this->id;
    }

    public function getWeight():int{
        return $this->weight;
    }

    public function setWeight(int $weight){
        $this->weight=$weight;
        return $this;
    }

    public function getName(){
        return $this->name;
    }

    public function setName(string $name){
        $this->name=$name;
        return $this;
    }

    public function setGazda(?Gazda $gazda){
        $this->gazda=$gazda;
    }

    public function getGazda(){
        return $this->gazda;
    }

    /**
     * @return Collection|Kezeles[]
     */
    public function getKezelesek(): Collection
    {
        return $this->kezelesek;
    }

    public function addKezelesek(Kezeles $kezelesek): self
    {
        if (!$this->kezelesek->contains($kezelesek)) {
            $this->kezelesek[] = $kezelesek;
            $kezelesek->setAllat($this);
        }

        return $this;
    }

    public function removeKezelesek(Kezeles $kezelesek): self
    {
        if ($this->kezelesek->removeElement($kezelesek)) {
            // set the owning side to null (unless already changed)
            if ($kezelesek->getAllat() === $this) {
                $kezelesek->setAllat(null);
            }
        }

        return $this;
    }

}