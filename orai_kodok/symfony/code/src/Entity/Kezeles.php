<?php

namespace App\Entity;

use App\Repository\KezelesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: KezelesRepository::class)]
class Kezeles
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'integer')]
    private $price;

    #[ORM\ManyToOne(targetEntity: Allat::class, inversedBy: 'kezelesek')]
    #[ORM\JoinColumn(nullable: false)]
    private $allat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAllat(): ?Allat
    {
        return $this->allat;
    }

    public function setAllat(?Allat $allat): self
    {
        $this->allat = $allat;

        return $this;
    }
}
