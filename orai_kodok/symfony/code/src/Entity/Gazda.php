<?php

namespace App\Entity;

use App\Repository\GazdaRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GazdaRepository::class)]
class Gazda
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $kor;

    #[ORM\OneToMany(targetEntity:'Allat', mappedBy:'gazda')]
    private Collection $allatok;

    #[ORM\OneToOne(mappedBy: 'gazda', targetEntity: Contact::class, cascade: ['persist', 'remove'])]
    private $contact;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getKor(): ?int
    {
        return $this->kor;
    }

    public function setKor(?int $kor): self
    {
        $this->kor = $kor;

        return $this;
    }

    public function getAllatok(){
        return $this->allatok;
    }

    public function addAllatok(Allat $allat){
        if(! $this->allatok->contains($allat)){
            $this->allatok[]=$allat;
        }
        return $this;
    }

    public function removeAllatok($allat){
        $this->allatok->removeElement($allat);
        return $this;
    }

    public function __toString()
    {
        return $this->getId()." ".$this->name;
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(Contact $contact): self
    {
        // set the owning side of the relation if necessary
        if ($contact->getGazda() !== $this) {
            $contact->setGazda($this);
        }

        $this->contact = $contact;

        return $this;
    }

}
