<?php

namespace App\Form;

use App\Entity\Allat;
use App\Entity\Gazda;
use App\Entity\Kezeles;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AllatFormType extends AbstractType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {

        $gazdaAttr = [];
        if ($builder->getData()->getGazda()) {
            $gazdaAttr["disabled"] = true;
        }
        $builder
            ->add('name')
            ->add('weight')
            ->add('gazda', EntityType::class, [
                'class' => Gazda::class,
                'attr' =>  $gazdaAttr
            ])
            ->add('kezelesek')
            ->add("submit", SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Allat::class
        ]);
    }
}
