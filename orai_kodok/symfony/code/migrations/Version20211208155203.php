<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211208155203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE allat ADD gazda_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE allat ADD CONSTRAINT FK_79C33CDC4E5C42F4 FOREIGN KEY (gazda_id) REFERENCES gazda (id)');
        $this->addSql('CREATE INDEX IDX_79C33CDC4E5C42F4 ON allat (gazda_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE allat DROP FOREIGN KEY FK_79C33CDC4E5C42F4');
        $this->addSql('DROP INDEX IDX_79C33CDC4E5C42F4 ON allat');
        $this->addSql('ALTER TABLE allat DROP gazda_id');
    }
}
