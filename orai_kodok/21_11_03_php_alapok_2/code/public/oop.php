<?php
require("Allat.php");


$allatok=[];

for ($i=0; $i < 100; $i++) { 
    $allatok[]=new Allat($i);
    $allatok[$i]->setNev("Allat Nev".$i);
    $allatok[$i]->setGazdiNeve("Gazdi Nev".$i);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <tbody>
    <?php foreach($allatok as $allat): ?>
        <tr>
            <td><?=$allat->meret?></td>
            <td><?=$allat->getNev()?></td>
            <td><?=$allat->getGazdiNeve()?></td>
        </tr>

    <?php endforeach ?>
    </tbody>
    </table>
</body>
</html>

