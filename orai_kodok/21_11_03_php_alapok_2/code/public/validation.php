<?php

$email = $_POST["email"];
if (isset($email)) {
    $email = trim($_POST["email"]);
    $email = filter_var($email, FILTER_SANITIZE_STRIPPED);
}
$emailCorrect = filter_var($email, FILTER_VALIDATE_EMAIL);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        .error {
            color: red
        }
    </style>
</head>

<body>
    <form method="POST">
        <label for="email" class="<?= !$emailCorrect ?  "error" : "" ?>">
            Email cím *
        </label>
        <input class="<?= !$emailCorrect ? "error" : "" ?>" id="email" name="email" type="text" value="<?= isset($email) ? $email : ""?>">
        <div>
            <?php if (!$emailCorrect) echo "nem helyes emailcímet adtál meg" ?>
        </div>
    </form>

</body>

</html>