<?php 
class Allat{
    // php 8 verzió
    public int $meret;
    
    // Regi verzió
    private $nev;

    function setNev($nev){
        if(!is_string($nev)){
            throw "Nem string";
        }
        $this->nev=$nev;
    }

    function getNev(){
        return $this->nev;
    }


// php 7+
    private $gazdiNeve;

    function setGazdiNeve(string $nev){
        $this->gazdiNeve=$nev;
    }

    function getGazdiNeve():string{
        return $this->gazdiNeve;
    }

    function __construct($meret)
    {
        $this->meret=$meret;
    }
}


// $allat = new Allat(2);

// var_dump($allat);

// // Beállítás elott gond
// //echo $allat->meret;
// echo "<br>";
// $allat->meret=10;

// echo $allat->meret;


// $allat->setNev("Nev");
// echo "<br>";
// echo $allat->getNev();

// echo "<br>";


// $allat->setGazdiNeve("Nev");
// echo $allat->getGazdiNeve();


?>