<?php

$dbserver = "db_server";
$db = "php_alap";
$nev = "app_user";
$jelszo = "t3rceS";
$port = "3306";
$dsn = "mysql:dbname=$db;host=$dbserver;port=$port;serverVersion=10.5";

$connection = null;
try {
    $connection = new PDO($dsn, $nev, $jelszo);
} catch (\Throwable $th) {
    echo $th->getMessage();
    var_dump($th->getTrace());
    throw "Nincs adatbázis";
}



if (isset($_POST["meret"]) && isset($_POST["nev"])) {
    // Ilyet ne (veszélyes)
    // $query = "INSERT into allat (meret, nev) VALUES (" . $_POST["meret"] . ",'" . $_POST["nev"] . "');";
    // $connection->exec($query);

    $query = "INSERT into allat (meret, nev) VALUES (:meret,:nev)";
    $statement = $connection->prepare($query);
    $statement->execute([
        "nev" => $_POST["nev"],
        "meret" => $_POST["meret"]
    ]);
}




if(isset($_GET["filter"])){
    $query = "select * from allat where nev like ?;";
    $statement=$connection->prepare($query);

    $statement->execute(["%".$_GET["filter"]."%"]);
    $data = $statement->fetchAll();
}else{

    $query = "select * from allat;";
    $data = $connection->query($query)->fetchAll();
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form method="post">
        <label for="nev">Név</label>
        <input type="text" name="nev" id="nev">
        <label for="meret">Méret</label>
        <input type="number" name="meret" id="meret">
        <input type="submit" value="add">
    </form>


    <form method="get">
        <input type="text" name="filter">
        <input type="submit" value="szűr">
    </form>

    <table>
        <tbody>
            <?php foreach ($data as $allat) : ?>
                <tr>
                    <td><?= $allat["id"]; ?></td>
                    <td><?= $allat["nev"]; ?></td>
                    <td><?= $allat["meret"]; ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</body>

</html>