<?php
$szamok = [1, 23, 3];

// f1
print_r(array_map(function ($e) {
    return $e * $e * $e;
}, $szamok));

print("<br>");
//f2

print_r(array_map(function ($e,$i) {
    return $e+$i+1;
},$szamok, array_keys($szamok)));


//f3
print("<br>");

$maganhangzok="aeoiu";

$szoveg="Hello";

$chars=str_split($szoveg);
$filtered=array_filter($chars,fn($char)=>!str_contains($maganhangzok,$char));
$kesz=implode($filtered);

print("<br>");
print($kesz);

// $szamok=[];
print("<br>");
print(array_reduce($szamok,fn($prev,$akt)=>$prev+$akt,0));

print("<br>");
print(array_reduce($szamok,fn($prev,$akt)=>$prev<$akt?$akt:$prev));


print("<br>");
$szamok[]=-5;
print_r($szamok);
$van=array_reduce($szamok,fn($prev,$akt)=>$prev || $akt<0,false);
print($van?"van":"nincs");



$matrix=[
    [2,2],
    [4,4]
];

