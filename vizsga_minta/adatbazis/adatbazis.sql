create table tanulo(
    tanulokod int NOT NULL primary key AUTO_INCREMENT,
    nev varchar(100) NOT NULL,
    szuldatum date DEFAULT NULL,
    anyja_neve varchar(100) NOT NULL,
    cim varchar(255) NOT NULL
);
create table targy(
    targykod int NOT NULL primary key AUTO_INCREMENT,
    targy_neve varchar(100) NOT NULL,
    tanar varchar(100) NOT NULL
);
create table jegyek(
    id int NOT NULL primary key AUTO_INCREMENT,
    targykod int DEFAULT NULL,
    tanulokod int DEFAULT NULL,
    jegy int DEFAULT NULL,
    foreign key (targykod) references targy(targykod),
    foreign key (tanulokod) references tanulo(tanulokod)
);


INSERT INTO tanulo (nev,szuldatum,anyja_neve,cim) values('Kiss Piroska', '1998.10.25', 'Tóth Kinga','8200 Veszprém, Balatoni út 203');
INSERT INTO tanulo (nev,szuldatum,anyja_neve,cim) values('Laczkó Imre','1997.06.23','Őri Diána','8200 Veszprém, József Attila utca 103');
INSERT INTO tanulo (nev,szuldatum,anyja_neve,cim) values('Balogh Helga','1998.12.01','Imre Piroska','8200 Veszprém, Kinizsi út 98');
INSERT INTO tanulo (nev,szuldatum,anyja_neve,cim) values('Vitéz Martin','1998.09.13','Mészáros Emese','8220 Balatonalmádi, Balaton part út 33');
INSERT INTO tanulo (nev,szuldatum,anyja_neve,cim) values('Czinege Zsuzsanna','1997.02.15','Balogh Éva','8220 Balatonalmádi, Mikszáth Kálmán utca 24');
INSERT INTO tanulo (nev,szuldatum,anyja_neve,cim) values('Papp Benedek','1997.03.03','Finta Annamária','8200 Veszprém, Budapesti út 420');
INSERT INTO tanulo (nev,szuldatum,anyja_neve,cim) values('Rácz Albert','1997.04.02','Módos Réka 8200','Veszprém, Bartók Béla út 12');

INSERT INTO targy (targy_neve,tanar) values('Hálózati ismeretek','Papp Bertalan');
INSERT INTO targy (targy_neve,tanar) values('Programozási ismeretek','Fülöp Katalin');
INSERT INTO targy (targy_neve,tanar) values('Matematika','Goczol Tamara');
INSERT INTO targy (targy_neve,tanar) values('Adatbázis-kezelés','Rácz Gusztáv');
INSERT INTO targy (targy_neve,tanar) values('Operációs rendszerek','Albert Nóra');


INSERT into jegyek(tanulokod,targykod,jegy) values(1,1,4);
INSERT into jegyek(tanulokod,targykod,jegy) values(1,2,5);
INSERT into jegyek(tanulokod,targykod,jegy) values(1,3,3);
INSERT into jegyek(tanulokod,targykod,jegy) values(2,1,5);
INSERT into jegyek(tanulokod,targykod,jegy) values(2,2,2);
INSERT into jegyek(tanulokod,targykod,jegy) values(2,4,3);
INSERT into jegyek(tanulokod,targykod,jegy) values(3,1,5);
INSERT into jegyek(tanulokod,targykod,jegy) values(3,4,4);
INSERT into jegyek(tanulokod,targykod,jegy) values(3,5,4);
INSERT into jegyek(tanulokod,targykod,jegy) values(4,1,3);
INSERT into jegyek(tanulokod,targykod,jegy) values(4,4,5);
INSERT into jegyek(tanulokod,targykod,jegy) values(5,1,3);
INSERT into jegyek(tanulokod,targykod,jegy) values(5,4,2);
INSERT into jegyek(tanulokod,targykod,jegy) values(6,1,2);
INSERT into jegyek(tanulokod,targykod,jegy) values(6,3,5);
INSERT into jegyek(tanulokod,targykod,jegy) values(7,2,5);
INSERT into jegyek(tanulokod,targykod,jegy) values(7,3,4);
INSERT into jegyek(tanulokod,targykod,jegy) values(7,4,5);
