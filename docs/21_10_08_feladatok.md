# Alap feladatok Tömbfüggvényekkel

A következő feladatokat **tömbműveletek** segítségével **KELL** megvalósítani

(Legfontosabbak szerszámaid a következők: map, filter, reduce, every, some, ...)

1. Egy számokat tartalmazó tömbből állítsd elő azt a tömböt, amely azok köbét tartalmazza!
2. Egy számokat tartalmazó tömbből állítsd elő azt a tömböt, amely minden számot annyival növel meg,
ahányadik elem a listában!
3. Számold meg, hogy hány magánhangzó van egy stringben!
4. Redukálás segítségével határozd meg egy számokat tartalmazó tömb összegét! (Ügyelj arra, hogy az
üres tömb összege nulla legyen!)
5. Redukálás segítségével határozd meg egy számokat tartalmazó tömb legnagyobb elemének értékét! (Lé-
nyegében a feladat Math.max() megírása tömb paraméterrel. Üres tömbre mi legyen az eredmény?)
6. Döntsd el (három különböző módon más-más tömbfüggvény használatával), hogy egy számokat tartal-
mazó tömbben van-e negatív szám! (Tipp: az egyik megvalósítás maga a some tömbfüggvény)
7. Döntsd el egy mátrixról, hogy minden eleme páros-e! (Egy lehetséges megoldás: baromi egyszerű lenne,
ha valahogyan ki tudnánk lapítani a mátrixot, így vektorra visszavezetve a feladatot!)

8. Készíts adatszerkezetet, amelyben az alábbi táblázatban látható adatokat tárolod! (A következő néhány
feladatot ezzel az adatszerkezettel oldd meg, lehetőleg minél tömörebben!)

|Név|azon|Átlag|
|-|-|-|
|Elte Mető|F3S5K2|2.04|
|Hát Izsák|K91FFG|4.37|
|Pál Inka|UWU431|5.00|

9. Mennyi a legrosszabb tanuló átlaga?
10. Add meg a legrosszabb átlagú tanuló összes adatát tartalmazó objektumot! (Több egyforma átlag esetén
elég egyet megadni, lásd: find tömbfüggvény.)
11. Mennyi az évfolyamátlag? (Magyarul az átlagok átlaga.)


Szorgalmi

1. Duplikátumszűrés. Készíts egy függvényt, amely tömbfüggvények használatával egy tömbből
eltávolít minden olyan elemet, amely egynél többször szerepel benne! A többször szereplő elemeket
egyszer sem kell megtartani, valamint az elemek sorrendje maradjon meg! (Segítség: Több út is járható.
Lehet pl. a sorozatnak csak egy részét nézni, hogy szerepel-e az aktuális elem az utána lévők között;
vagy létezik külön tömbfüggvény elölről és hátulról induló keresésre.)
Például: [5, 6, 3, 2, 3, 4, 5] → [6, 2, 4]
2. Madárnyelv. Készítsd el azt a függvényt tömbfüggvények és -műveletek használatával, amely egy pa-
raméterül kapott – egyszerűség kedvéért csak kisbetűs – szöveget madárnyelvre fordítva ad vissza! A
madárnyelvet úgy kell képezni, hogy minden magánhangzó után beszúrunk egy w betűt, majd megis-
mételjük az adott magánhangzót. (Lásd a példákat!)
Például: "u" → "uwu"
Például: "elte ik" → "eweltewe iwik"
Például: "de szép bauxit, honnan van?" → "dewe széwép bawauwuxiwit, howonnawan vawan?"
3. Nullás sorok. Tömbfüggvények használatával határozd meg, hogy egy számokat tároló mátrixban
hány olyan sor van, ami csak nullákat tartalmaz!
4. Szótár. Adott egy többnyelvű szótár, amely a következő formátumban tartalmazza az egyes szavak
különböző nyelvű alakjait:
```js
let dict = [
    {
        en: "moon",
        de: "der Mond",
        hu: "hold",
        rs: "mesec"
    },
    {
        en: "fox",
        de: "der Fuchs",
        hu: "róka",
        rs: "lisica"
    },
    {
        en: "girl",
        de: "das Mädchen",
        hu: "lány",
        rs: "devojka"
    }
];
```
Készítsd el azt a translate(word, from, to) szignatúrájú függvényt, amely a from kódú nyelven
megadott word szót lefordítja to nyelvre! Kezeld azt az esetet is, amikor nem létező szót vagy nyelvet
adott meg a felhasználó! (pl. undefined visszatérési érték)

```js
Például: translate("fox", "en", "hu") → "róka"
Például: translate("der Mond", "de", "jp") → undefined
Például: translate("cat", "en", "hu") → undefined
```

# Alap dom műveletek

1. Legyen egy HTML oldalon egy Köszönj! gomb! Rákattintva maga a gomb tűnjön el, és jelenjen meg
jó nagy betűkkel a Helló Világ! felirat!
2. Gondoljon a gép egy 0 és 100 közötti egész számra! (Tippek: Math.random() és Math.floor()) A mi
feladatunk, hogy kitaláljuk a számot. Legyen lehetőség tippelni a számra, a gép pedig annyit válaszoljon,
hogy az általa gondolt szám kisebb vagy nagyobb az általunk tippeltnél.
3. Egy szöveges beviteli mezőben legyen lehetőség megadni egy interneten elérhető kép URL-jét! Egy
mellette lévő gombra kattintva jelenítsd meg a képet a dokumentumban!
4. Egy oldal betöltésekor listázd ki az oldal aljára (sorszámozatlan listaként megvalósítva) az összes raj-
ta található hiperhivatkozás (link) címét! (Természetesen ehhez először célszerű néhány hivatkozást
elrejteni az oldal különböző pontjain, hogy ellenőrizni tudjuk a működést.)
5. Egy oldalon adott egy számlaegyenleg (pl. 500 000 Ft kezdőértékkel), egy szövegbeviteli mező és egy
gomb. A gombot megnyomva vonjuk le az egyenlegből a mezőbe írt összeget! Minden input eseménykor
ellenőrizzük a mező tartalmát, és ha nem szám van benne vagy a szám nagyobb mint a pillanatnyi
egyenleg, kapjon a mező error stílusosztályt! (Értelemszerűen a stílusosztályt vegyük is le róla, ha
helyesre változik a tartalma.)
6. Adott egy három oszlopból álló táblázat, amely fölött elhelyeztünk három szöveges mezőt és egy gombot.
A gombra kattintva a három beviteli mező értéke új sorként szúródjon be a táblázat végére, és a mezők
álljanak vissza üres állapotba!
7. Helyezz fel az oldalra egy csúszkát (input type="range"), amely a lapot teljes szélességében kitölti!
Érjük el (mondjuk egy doboz/div megfelelő pozícionálásával), hogy a csúszka értékét változtatva mindig
alatta (folyamatosan mozogva) mutassa, hogy hány százalékon áll! Az egyszerűség kedvéért ebben a
feladatban a csúszka terjedhet 0-tól 100-ig.
