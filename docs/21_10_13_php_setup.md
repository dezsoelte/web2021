# Php setup

## xampp

- [xampp telepítő](https://www.apachefriends.org/index.html)
- [composer](https://thecodedeveloper.com/install-composer-windows-xampp/)

## Docker

- [docker telepítő](https://docs.docker.com/desktop/windows/install/)

Telepítés és újraindítás után nyiss egy parancssort a docker_base mappában, és add ki a `docker-compose up` parancsot.

Ezután a weboldal elérhető lesz a [http://localhost:8080](http://localhost:8080) címen.

Az adatbázis admin pedig a [http://localhost:8081](http://localhost:8081) címen.

Az adatbázis jelszó illetve az elérési portok a docker_base/.env fileban találhatók/állíthatók