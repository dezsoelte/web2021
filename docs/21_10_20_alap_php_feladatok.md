# Php gyakorló feladatok

## Alap műveletek

1. Írjuk meg ciklussal és rekurzióval is azt a függvényt, amely kiszámítja egy adott egész szám faktoriálisát!
(Mi történik, ha nem egészet adunk neki?)
2. Írassuk ki egyre kisebb címsorokkal (h1-től h6-ig) a Helló világ! szöveget!
3. Adott (beégetett) egy egész számokat tartalmazó tömb. Válogassuk ki belőle a páros számokat, majd
emeljük őket négyzetre!
4. Tömbfüggvények tekintetében a PHP-től sem teljesen idegenek a már JavaScriptből ismert map, filter
és reduce függvények megfelelői, azonban nagyon hiányzik nekünk az every. Írjuk meg az array_every
függvényt, amely szigorúan egy tömböt és egy függvényt (callable) kapva eldönti, hogy a callable a
tömb minden elemére igazat ad-e! A feladatot oldjuk meg legalább kétféleképpen: foreach ciklus és
iterátorfüggvények használatával is!
5. Adott egy hibaüzeneteket tartalmazó tömb. Jelenítsük meg a hibalistát felsorolásként!
6. Készítsünk tesztrendszert! Egy változóban el kell tárolni a kérdések listáját, az egyes kérdésekhez tartozó
lehetséges válaszokat (amelyek betűjele is megadható) és a helyes válasz jelét. Generáljunk HTML
űrlapot, ahol jelölve van a helyes megoldás!
7. Egy hallgatói nyilvántartásban minden hallgatóról tároljuk a nevét, a neptun azonosítóját, a születési
évét és nemét. Oldjuk meg az alábbi feladatokat:
    1.  Készítsük el az adatszerkezetet és töltsük fel néhány hallgatóval!
    2.  Táblázat formájában jelenítsük meg a hallgatói nyilvántartást!
    3. Írjuk ki, hogy ki a legidősebb hallgató!
    4. Ha 1970 előtt született a legidősebb hallgató, akkor írjuk ki mellé, hogy az egész életen át tartó
tanulás példaképe.
    5. Döntsük el a nyilvántartás alapján, hogy van-e lány a hallgatók között!
    6. Két csík formájában tüntessük fel a fiúk és lányok darabszámát!

## "Összetettebb" feladatok natúr php

```php
    $data = [
        [
            "name" => "Kiss Jenő",
            "neptun" => "ABC123",
            "year" => 1996,
            "gender" => "férfi"
        ],
        [
            "name" => "Nagy Klára",
            "neptun" => "XYZ999",
            "year" => 1950,
            "gender" => "nő"
        ],
        [
            "name" => "Erős István",
            "neptun" => "IDDQD8",
            "year" => 1994,
            "gender" => "férfi"
        ]
    ];
```

1. Egy hallgatói nyilvántartásban minden hallgatóról tároljuk a nevét, a neptun azonosítóját, a születési
évét és nemét. Oldjuk meg az alábbi feladatokat:
    1. Táblázat formájában jelenítsük meg a hallgatói nyilvántartást!
    2. Írjuk ki, hogy ki a legidősebb hallgató!
    3. Ha 1970 előtt született a legidősebb hallgató, akkor írjuk ki mellé, hogy az egész életen át tartó
tanulás példaképe.
    4. Döntsük el a nyilvántartás alapján, hogy van-e lány a hallgatók között!
    5. Két csík formájában tüntessük fel a fiúk és lányok darabszámát!
2. Készíts olyan oldalt, amelynek paraméterül adva egy nevet, üdvözli a felhasználót! A paramétert a
URL-ben várjuk, de legyen egy űrlap is, ahol lehet módosítani! Az űrlap induljon egy alapértelmezett
értékről, majd tartsa meg az állapotát!
3. Készíts elsőfokú egyenletet (ax + b = 0) megoldó oldalt! Űrlapon keresztül lehessen megadni a és b
értékét! A megoldást az űrlap alá írjuk! Gondoskodj az űrlap állapottartásáról is! Kezeljük azt az esetet
is, ha a felhasználó szöveget ír be vagy nullával osztana!
4. Készítsünk regisztrációs űrlapot, amelyet POST metódussal küldünk el! Az űrlap legyen állapottartó,
és ellenőrizzük a hibákat! A hibaüzeneteket a hibás mező mellett jelenítsük meg! Az űrlap mezői és
feltételek:
    - Teljes név: szövegbevitel, kötelező, legalább két szó
    - E-mail: szövegbevitel, kötelező, valid e-mail
    - TAJ szám: szövegbevitel, pontosan 9 karakter, csak számjegyek
    - Életkor: szövegbevitel, egész szám, pozitív
    - Nem: rádió gombok, férfi vagy nő, kötelező
    - Regisztrációs dátum: dátummező, kötelező, alapértelmezetten a mai dátum
    - Elfogadja a feltételeket: jelölőmező, kötelező
    - Megjegyzés: hosszú szöveg, lehet üres
