<?php
require("fejlec.html");
$dbserver = "localhost";
$db = "vizsga";
$nev = "root";
$jelszo = "";
$port = "3306";
$dsn = "mysql:dbname=$db;host=$dbserver;port=$port";

$connection = null;
try {
    $connection = new PDO($dsn, $nev, $jelszo);
} catch (\Throwable $th) {
    echo $th->getMessage();
    var_dump($th->getTrace());
    throw "Nincs adatbázis";
}

// SELECT targy.targy_neve, AVG(jegyek.jegy) 
// FROM targy
// 	JOIN jegyek on targy.targykod=jegyek.targykod 
// GROUP by targy.targykod, targy.targy_neve
$query = "SELECT targy.targy_neve as nev, AVG(jegyek.jegy) as atl 
    FROM targy JOIN jegyek on targy.targykod=jegyek.targykod 
    GROUP by targy.targykod, targy.targy_neve
    order by AVG(jegyek.jegy) desc";
$atlagok = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);


// SELECT tanulo.nev, AVG(jegyek.jegy) AS atl 
// FROM tanulo JOIN jegyek ON tanulo.tanulokod=jegyek.tanulokod
// GROUP BY tanulo.tanulokod, tanulo.nev
// HAVING AVG(jegyek.jegy)>4
// ORDER BY AVG(jegyek.jegy) DESC

$query = "SELECT tanulo.nev as nev, AVG(jegyek.jegy) AS atl 
    FROM tanulo JOIN jegyek ON tanulo.tanulokod=jegyek.tanulokod
    GROUP BY tanulo.tanulokod, tanulo.nev
    HAVING AVG(jegyek.jegy)>4
    ORDER BY AVG(jegyek.jegy) DESC;
    ";
$tanuloAtlag = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);




?>
<div class="container">



    <h3 class="text-center">Tárgyak átlaga</h3>
    <table class="table">
        <tbody>
            <tr><th>Targy</th><th>atlag</th></tr>
            <?php foreach($atlagok as $atlag): ?>
                <tr>
                    <td><?=$atlag["nev"]?></td>
                    <td><?=$atlag["atl"]?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

    <h3 class="text-center">Jó tanulok átlaga</h3>
    <table class="table">
        <tbody>
            <tr><th>Tanulo</th><th>atlag</th></tr>
            <?php foreach($tanuloAtlag as $atlag): ?>
                <tr>
                    <td><?=$atlag["nev"]?></td>
                    <td><?=$atlag["atl"]?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>


</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
</body>
</html>