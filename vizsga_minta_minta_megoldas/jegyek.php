<?php
require("fejlec.html");


$dbserver = "localhost";
$db = "vizsga";
$nev = "root";
$jelszo = "";
$port = "3306";
$dsn = "mysql:dbname=$db;host=$dbserver;port=$port";

$connection = null;
try {
    $connection = new PDO($dsn, $nev, $jelszo);
} catch (\Throwable $th) {
    echo $th->getMessage();
    var_dump($th->getTrace());
    throw "Nincs adatbázis";
}


$query = "select * from tanulo;";
$tanulok = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);


$query = "select * from targy;";
$targyak = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);


// •	Legördülő lista, amelyben az osztályzatot lehet kiválasztani, elégtelentől a jeles osztályzatig (statikusan létrehozva).
$jegyek = [
    1 => "elegtelen",
    2 => "elégséges",
    3 => "kozepes",
    4 => "jo",
    5 => "kivalo"
];

$message="";


// Elküldtük e az adatot
if ($_SERVER["REQUEST_METHOD"] === "POST") {


    // •	Amennyiben valamelyik adat nem kerül megadásra a hiba üzenetet: „Kérem minden adatot adjon meg!”. 
    if (!empty($_POST["tanulo"]) && !empty($_POST["targy"]) && !empty($_POST["jegy"])) {

        $targy=$_POST["targy"];
        $jegy=$_POST["jegy"];
        $tanulo=$_POST["tanulo"];


        // van e ilyen az adatbázisban
        // •	Vizsgáljuk meg, hogy a megadott tanulónak a kiválasztott tantárgyból van-e már jegye. 
        // Ha van, hibaüzenet: „A megadott tanulónak a kiválasztott tantárgyból már van jegye”. 
        $query = "select * from jegyek where targykod=$targy and tanulokod=$tanulo;";
        $vaneEJegy = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
        if(count($vaneEJegy)===0){

            
            $query = "insert into jegyek (targykod,tanulokod,jegy) values ($targy,$tanulo,$jegy);";
            $connection->exec($query);
            $message="Sikerült menteni a jegyet";
        }else{
            $message="A megadott tanulónak a kiválasztott tantárgyból már van jegye";
        }


    }else{
        $message="Kérem minden adatot adjon meg!";
    }
}

?>




<div class="container">
    <form method="POST">
<!-- 
   Egy legördülő lista, amely a nyilvántartásban szereplő tanulókat foglalja magába. 
    A listát statikusan hozza létre, de a lista tartalmát dinamikusan generálja le.
     A böngészőben a tanulók nevei jelenjenek meg. -->
        <div class="form-group">
            <label for="tanulo">tanulo</label>
            <select name="tanulo" id="tanulo" class="form-control">
                <option disabled selected>Kérlek válasz értéket</option>
                <?php foreach ($tanulok as $tanulo) : ?>
                    <option value=<?= $tanulo["tanulokod"] ?> >
                        <?= $tanulo["nev"] ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
<!--         
        •	Egy legördülő lista, amely a nyilvántartásban szereplő tantárgyakat foglalja magába. 
        A listát statikusan hozza létre, de a lista tartalmát dinamikusan generálja le.
         A böngészőben a tantárgyak nevei jelenjenek meg. -->
        <div class="form-group">
            <label for="targy">Tantargy</label>
            <select name="targy" id="targy" class="form-control">
                <option disabled selected>Kérlek válasz értéket</option>
                <?php foreach ($targyak as $targy) : ?>
                    <option value=<?= $targy["targykod"]  ?> >
                        <?= $targy["targy_neve"] ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>

        <!-- •	Legördülő lista, amelyben az osztályzatot lehet kiválasztani, elégtelentől a jeles osztályzatig (statikusan létrehozva). -->
        <div class="form-group">
            <label for="jegy">Jegy</label>
            <select name="jegy" id="jegy" class="form-control">
                <option disabled selected>Kérlek válasz értéket</option>
                <?php foreach ($jegyek as $jegyszam => $jegynev) : ?>
                    <option value=<?= $jegyszam ?>>
                        <?= $jegynev ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>


        <input type="submit" value="lead" class="form-control">
    </form>


<div><?= $message?></div>


</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
</body>

</html>